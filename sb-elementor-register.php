<?php
/**
 * Plugin Name: Elementor User Register form
 * Description: Add an register form to insert into pages
 * Plugin URI: https://gitlab.com/fullstackpress/elementor-user-register
 * Version: 0.1.0
 * Author: Samuel Bengtsson, Jesse Tuttle
 * Author URI: https://gitlab.com/fullstackpress/
 * Text Domain: sb-elementor
 */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

final class Elementor_Register_Extension
{
    /**
     * Plugin Version
     *
     * @since 0.1.0
     *
     * @var string Elementor User Register Version
     */
    const VERSION = '0.1.0';

    /**
     * Minimum Elementor Version
     *
     * @since 0.1.0
     *
     * @var string Minimum Elementor version required to run the plugin.
     */
    const MINIMUM_ELEMENTOR_VERSION = '2.4.0';

    /**
     * Minimum PHP Version
     *
     * @since 0.1.0
     *
     * @var string Minimum PHP version required to run the plugin.
     */
    const MINIMUM_PHP_VERSION = '7.0';

    /**
     * Instance
     *
     * @since  0.1.0
     *
     * @access private
     * @static
     *
     * @var Elementor_Register_Extension The single instance of the class.
     */
    private static $_instance = null;

    /**
     * Instance
     *
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since  0.1.0
     *
     * @access public
     * @static
     *
     * @return Elementor_Register_Extension An instance of the class.
     */
    public static function instance()
    {

        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * Elementor_Register_Extension constructor.
     *
     * @since  0.1.0
     *
     * @access public
     */
    public function __construct()
    {
        add_action('init', [$this, 'i18n']);
        add_action('plugins_loaded', [$this, 'init']);
    }

    /**
     * Load Textdomain
     *
     * Load plugin localization files.
     *
     * Fired by `init` action hook.
     *
     * @since  0.1.0
     *
     * @access public
     */
    public function i18n()
    {
        load_plugin_textdomain('sb-elementor');
    }

    public function init()
    {
        require_once(__DIR__ . '/src/Admin_Notices.php');
        $admin_notices = new Admin_Notices;
        // Check if Elementor installed and activated
        if (!did_action('elementor/loaded')) {
            add_action('admin_notices', [$admin_notices, 'admin_notice_missing_main_plugin']);
            return;
        }

        // Check for required Elementor version
        if (!version_compare(ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=')) {
            add_action('admin_notices', [$admin_notices, 'admin_notice_minimum_elementor_version']);
            return;
        }

        // Check for required PHP version
        if (version_compare(PHP_VERSION, self::MINIMUM_PHP_VERSION, '<')) {
            add_action('admin_notices', [$admin_notices, 'admin_notice_minimum_php_version']);
            return;
        }

        // Add Plugin actions
        add_action('elementor/widgets/widgets_registered', [$this, 'init_widgets']);
        add_action('elementor/controls/controls_registered', [$this, 'init_controls']);
        add_action('elementor/frontend/after_enqueue_styles', [$this, 'widget_styles']);
        add_action('elementor/frontend/after_register_scripts', [$this, 'widget_scripts']);

        add_action('rest_api_init', function () {
            register_rest_route('sb/custom/', '/register', [
                'methods'  => 'POST',
                'callback' => [$this, 'endPoint'],
            ]);
            register_rest_route('sb/custom/', '/get/states', [
                'methods'  => 'POST',
                'callback' => [$this, 'getStates'],
            ]);
        });
    }

    public function widget_styles()
    {
        wp_enqueue_style('widget-elementor-register', plugins_url('css/sb-elementor-style.css', __FILE__));
    }

    public function widget_scripts()
    {
        wp_enqueue_script('widget-elementor-register-scripts', plugins_url('js/sb-elementor-register.js', __FILE__), [], '1.0', true);
    }

    public function init_widgets()
    {
        require_once(__DIR__ . '/src/Elementor_Register_Widget.php');
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Elementor_Register_Widget());
    }

    public function init_controls()
    {

    }

    /**
     * @param WP_REST_Request $request
     *
     * @return WP_REST_Response
     */
    public function endPoint($request)
    {
        $errors        = [];
        $email         = $request->get_param('email');
        $reEmail       = $request->get_param('re-email');
        $pass          = $request->get_param('password');
        $rePass        = $request->get_param('re-password');
        $displayName   = $request->get_param('username');
        $phone         = $request->get_param('phone');
        $address       = $request->get_param('address1');
        $address2      = $request->get_param('address2');
        $city          = $request->get_param('city');
        $zip           = $request->get_param('zip');
        $country       = $request->get_param('country');
        $newsletter    = $request->get_param('newsletter');
        $product       = $request->get_param('product');
        $firstname     = $request->get_param('firstname');
        $lastname      = $request->get_param('lastname');
        $state         = $request->get_param('state');
        $companyName   = $request->get_param('company_name');
        $auto_username = $request->get_param('auto_username');
        $redirectUrl   = $request->get_param('redirect_url');
        $userRole      = $request->get_param('user_role');
        if ($this->isSpam($email, $firstname . ' ' . $lastname)) {
            return new \WP_REST_Response(['success' => false, 'message' => ['Sorry, but spam was detected.'], 'url' => ''], 200);
        }
        if ($reEmail && $email !== $reEmail) {
            $errors[] = 'Email does not match';
        }
        if ($pass !== $rePass && $pass && $rePass) {
            $errors[] = 'Password does not match';
        }
        if ($errors) {
            return new \WP_REST_Response(['success' => false, 'message' => $errors, 'url' => ''], 200);
        }
        if ($auto_username === 'yes') {
            $userName = $this->get_radnom_unique_username();
        } else {
            if ($displayName) {
                $userName = $displayName;
            } else {
                $user     = explode('@', $email);
                $userName = sanitize_user($user[0]);
            }
        }
        if (username_exists($userName)) {
            return new \WP_REST_Response(['success' => false, 'message' => ['Username already taken'], 'url' => ''], 200);

        }

        if (!$pass) {
            $pass = wp_generate_password(12);
        }
        $userId = wp_create_user($userName, $pass, $email);
        if (is_wp_error($userId)) {
            return new \WP_REST_Response(['success' => false, 'message' => [$userId->get_error_message()], 'url' => ''], 200);
        }
        update_user_meta($userId, 'nickname', $displayName);
        if($userRole){
            wp_update_user(['ID' => $userId, 'display_name' => $displayName, 'first_name' => $firstname, 'last_name' => $lastname, 'role' => $userRole]);
        }else{
            wp_update_user(['ID' => $userId, 'display_name' => $displayName, 'first_name' => $firstname, 'last_name' => $lastname]);
        }
        add_user_meta($userId, 'billing_address_1', $address);
        add_user_meta($userId, 'billing_city', $city);
        add_user_meta($userId, 'billing_postcode', $zip);
        add_user_meta($userId, 'billing_country', $country);
        add_user_meta($userId, 'billing_phone', $phone);
        add_user_meta($userId, 'billing_email', $email);
        add_user_meta($userId, 'shipping_address_1', $address);
        add_user_meta($userId, 'billing_state', $state);
        add_user_meta($userId, 'shipping_state', $state);
        if ($address2) {
            add_user_meta($userId, 'shipping_address_2', $address2);
            add_user_meta($userId, 'billing_address_2', $address2);
        }
        add_user_meta($userId, 'shipping_city', $city);
        add_user_meta($userId, 'shipping_postcode', $zip);
        add_user_meta($userId, 'shipping_country', $country);
        add_user_meta($userId, 'billing_first_name', $firstname);
        add_user_meta($userId, 'billing_last_name', $lastname);
        add_user_meta($userId, 'shipping_first_name', $firstname);
        add_user_meta($userId, 'shipping_last_name', $lastname);
        add_user_meta($userId, 'shipping_company', $companyName);
        add_user_meta($userId, 'billing_company', $companyName);
        if ($product) {
            if ($address && $city && $firstname && $lastname && $country) {
                $order = wc_create_order();
                $p     = wc_get_product($product);
                $p->set_price(0);
                $order->add_product($p, 1);
                $order->set_customer_id($userId);
                $shipping = [
                    'first_name' => $firstname,
                    'last_name'  => $lastname,
                    'email'      => $email,
                    'phone'      => $phone,
                    'address_1'  => $address,
                    'city'       => $city,
                    'state'      => $state,
                    'postcode'   => $zip,
                    'country'    => $country,
                ];
                $order->set_address($shipping, 'billing');
                $order->set_address($shipping, 'shipping');
                $order->calculate_totals();

                $order->update_status("processing", 'Register free product', false);
            } else {
                require_once(ABSPATH . 'wp-admin/includes/user.php');
                wp_delete_user($userId);
                return new \WP_REST_Response(['success' => false, 'message' => ['Details are required for free gift.'], 'url' => ''], 200);
            }
        }
        if (!$rePass) {
            $this->sendMail($userId, '', true);
            return new \WP_REST_Response(['success' => true, 'message' => __('Please check your e-mail for login information.'), 'url' => ''], 200);

        } else {
            $this->sendMail($userId, $pass);
            wp_set_auth_cookie($userId, true);
            if($redirectUrl){
                $url = get_permalink(intval($redirectUrl));
            }else{
                $url = get_home_url();
            }
            return new \WP_REST_Response(['success' => true, 'message' => [], 'url' => $url], 200);

        }

    }

    /**
     * @param WP_REST_Request $request
     *
     * @return string
     */
    public function getStates($request)
    {
        require_once(__DIR__ . '/src/Elementor_Register_Widget.php');
        $widget     = new Elementor_Register_Widget;
        $required   = $request->get_param('required');
        $size       = $request->get_param('size');
        $showLabels = $request->get_param('showLabels');
        $gapData        = $request->get_param('gap');
        $gap['size'] = $gapData;
        $country    = $request->get_param('country');
        return $widget->renderStateForm('state', 'State / Province / Region', $required, $size, $showLabels, $gap, $country);
    }

    private function get_radnom_unique_username()
    {
        $user_exists = 1;
        do {
            $rnd_str     = sprintf("%0d", mt_rand(1, 999999999));
            $user_exists = username_exists($rnd_str);
        } while ($user_exists > 0);
        return $rnd_str;
    }

    private function sendMail($userId, $pass, $newPass = false)
    {
        global $wpdb, $wp_hasher;
        $user       = new WP_User($userId);
        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);
        $message    = sprintf(__('New user registration on your blog %s:'), get_option('blogname')) . "rnrn";
        $message    .= sprintf(__('Username: %s'), $user_login) . "rnrn";
        $message    .= sprintf(__('E-mail: %s'), $user_email) . "rn";
        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);
        $switched_locale = false;
        if ($newPass) {
            $key = wp_generate_password(20, false);
            do_action('retrieve_password_key', $user->user_login, $key);

            // Now insert the key, hashed, into the DB.
            if (empty($wp_hasher)) {
                require_once ABSPATH . WPINC . '/class-phpass.php';
                $wp_hasher = new PasswordHash(8, true);
            }
            $hashed = time() . ':' . $wp_hasher->HashPassword($key);
            $wpdb->update($wpdb->users, ['user_activation_key' => $hashed], ['user_login' => $user->user_login]);

            $switched_locale = switch_to_locale(get_user_locale($user));

            $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
            $message .= __('To set your password, visit the following address:') . "\r\n\r\n";
            $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . ">\r\n\r\n";
            $message .= wp_login_url() . "\r\n";
        } else {
            $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
            $message .= sprintf(__('Password: %s'), $pass) . "\r\n\r\n\"";
            $message .= __('To login, visit the following address:') . "\r\n\r\n";
            $message .= wp_login_url() . "\r\n";
        }
        $wp_new_user_notification_email = [
            'to'      => $user->user_email,
            /* translators: Password change notification email subject. %s: Site title */
            'subject' => __(get_option('blogname') . ' Your username and password info'),
            'message' => $message,
            'headers' => '',
        ];
        $wp_new_user_notification_email = apply_filters('wp_new_user_notification_email', $wp_new_user_notification_email, $user, $blogname);

        wp_mail(
            $wp_new_user_notification_email['to'],
            wp_specialchars_decode(sprintf($wp_new_user_notification_email['subject'], $blogname)),
            $wp_new_user_notification_email['message'],
            $wp_new_user_notification_email['headers']
        );

        if ($switched_locale) {
            restore_previous_locale();
        }
    }

    private function isSpam($email = '', $name = '')
    {
        $isSpam = FALSE;

        if (function_exists('akismet_init')) {

            $wpcom_api_key = get_option('wordpress_api_key');
            $content       = [];
            if (!empty($wpcom_api_key)) {

                global $akismet_api_host, $akismet_api_port;

                // set remaining required values for akismet api
                $content['user_ip']              = preg_replace('/[^0-9., ]/', '', $_SERVER['REMOTE_ADDR']);
                $content['user_agent']           = $_SERVER['HTTP_USER_AGENT'];
                $content['referrer']             = $_SERVER['HTTP_REFERER'];
                $content['blog']                 = get_option('home');
                $content['comment_author']       = $name;
                $content['comment_author_email'] = $email;
                if (empty($content['referrer'])) {
                    $content['referrer'] = get_permalink();
                }

                $queryString = '';

                foreach ($content as $key => $data) {
                    if (!empty($data)) {
                        $queryString .= $key . '=' . urlencode(stripslashes($data)) . '&';
                    }
                }

                $i = 0;
                do {
                    $response = akismet_http_post($queryString, $akismet_api_host, '/1.1/comment-check', $akismet_api_port);
                    if (empty($response)) {
                        sleep(1);
                    };
                } while (($i < 6) and (empty($response)));

                if (empty($response)) {
                    $isSpam = FALSE;
                }

                if (!empty($response) && $response[1] == 'true') {
                    update_option('akismet_spam_count', get_option('akismet_spam_count') + 1);
                    $isSpam = TRUE;
                }

            }

        }

        return $isSpam;
    }
}

Elementor_Register_Extension::instance();