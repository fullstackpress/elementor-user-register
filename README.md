# Elementor User Register Form #

**Version:** 0.1.0  
**Developers:** [Samuel Bengtsson](https://profiles.wordpress.org/zarex360/), [Jesse Tuttle](https://profiles.wordpress.org/afknet/)  
**Website:** [https://gitlab.com/fullstackpress/elementor-user-register/](https://gitlab.com/fullstackpress/elementor-user-register/)  
**Tags:** wordpress, elementor, user registration  
**Tested up to WordPress:** 5.1  
**License:** GPLv3  
**License URI:** https://www.gnu.org/licenses/gpl-3.0.html  

## Description ##

Elementor User Registration by [FullStackPress](https://gitlab.com/fullstackpress/) provides an [Elementor](https://wordpress.org/plugins/elementor/) Block for User Registration. Support for WooCommerce User Details and Akismet spam protection are built in but are not required.

## Features ##

* Elementor Block built to match the "Login" block provided in [Elementor Pro](https://elementor.com/pricing/). Please Note: Elementor Pro is NOT required to use this plugin. 
* Akismet Spam Protection: If akismet is activated, user registrations will be checked thru the Akismet Spam Filter. If a spammer is detected, the site will provide the user a registration error. (Akismet subscription required.)
* Generate Secure Username: A function to provide an auto-generated, random, username of 9 digits.
* New User Free Gift: Ability to provide new users a free gift at the time of registration. Admin can provide products that are available for the user to select one (1) at the time of registration. Order will be generated and placed into the system as "Pending". Requires: WooCommerce
* Required Consent: Terms & Conditions, Privacy Policy, DMCA Disclaimer apprval options are available.

## Installation ##

### Minimum Requirements ###

* WordPress 5.0 or greater
* Elementor 2.4.0 or greater
* PHP version 7.0 or greater

### Installation ###

1. Install using the WordPress built-in Plugin installer, or Extract the zip file and drop the contents in the `wp-content/plugins/` directory of your WordPress installation.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Go to the Page you desire to add the User Registration Form on.
4. Press the 'Edit with Elementor' button.
5. Now you can drag and drop the User Registration widget from the left panel onto the content area. 

## Frequently Asked Questions ##

**Is WooCommerce or Akismet required for this plugin to work?**

No.

WooCommerce is only required if you would like to:
1. Collect user's personal data, such as data needed for WooCommerce orders.  
2. Provide a free gift to users at time of registration.

Akismet is only required if you would like spam protection for user registration. (Akismet subscription required.)

**How does "Generate Secure Username" work?**

Generate Secure Username function replaces the auto-generated or user submitted Username with a 9 digits randomly generated username. The name is verified to be unique and not start with a 0. This provides an additional level of privacy for users since usernames are not able to be changed.

**How does the "New User Free Gift" work?**

Ability to provide new users a free gift at the time of registration. Admin can provide products that are available for the user to select one (1) at the time of registration. Order will be generated and placed into the system as "Pending". Requires: WooCommerce

## Planned Development ##

We are currently working on the following points:
*  Full Elementor Styling Experience with foreground/background coloring, styling and typeface settings.
*  Ability for Admin to set new user default role: Subscriber / Customer
*  Support for WooMemberships, ability for Admin to set use user default membership group.
*  Email Newsletter Subscription support at time of User Registration. Priority to add: MailChimp, SendInBlue

## Changelog ##

### 0.1.0 - 2019-03-15 ###
* Initial Public Beta Release

### 0.1.0 - 2019-03-18 ###
* Added setting for redirect uri
* Added setting for user role on register