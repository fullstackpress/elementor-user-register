(function ($) {

    var handleRegister = function (e) {
        e.preventDefault();
        var $form = $(this);
        var nonce = $(this).data('nonce');
        var data = $(this).serialize();
        var base = $(this).data('base');
        var url = base + '/wp-json/sb/custom/register?_wpnonce=' + nonce;
        $.ajax({
            url: url,
            data: data,
            method: 'post'
        })
            .done(function (response) {
                if (response.success) {
                    if (response.url) {
                        window.location.replace(response.url);
                    } else {
                        $form.hide();
                        $('#success-register').show();
                        $('#success-register').html('<p>' + response.message + '</p>');

                    }
                } else {
                    $('#errors').html('');
                    $('#errors').html('<p>' + response.message[0] + '</p>');
                }
            });


    };
    var handleCountryChange = function () {
        var country = $(this).val();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var required = $(this).data('required');
        var size = $(this).data('size');
        var showLabels = $(this).data('showlabels');
        var gap = $(this).data('gap');
        var nonce = $(this).data('nonce');
        var base = $(this).data('base');
        var url = base + '/wp-json/sb/custom/get/states?_wpnonce=' + nonce;
        var data = {'country': country, 'id' : id, 'name': name, 'required': required, 'size':size, 'showLabels': showLabels, 'gap':gap};
        $.ajax({
            url: url,
            data: data,
            method: 'post'
        }).done(function (response) {
            console.log(response);
            $('#state-select').replaceWith(response);
        })

    };
    var addRadioCheck = function () {
        var parent = $(this).parent();
        $('.custom-parent').css('outline', 'none');
        parent.css('outline', '1px solid red');
    };

    $(document).on('ready', function () {
        $('#custom_register_form').on('submit', handleRegister);
        $('.custom-radio-check').on('change', addRadioCheck);
        $('#country').on('change', handleCountryChange);
    });
}(jQuery));